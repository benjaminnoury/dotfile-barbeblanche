# Réglage de base pour les logiciels
# vi:ft=perl
$pdf_previewer = "zathura";
$pdflatex = 'pdflatex -shell-escape -enable-write18 -interaction=nonstopmode -file-line-error';
$latex = 'pdflatex -shell-escape -enable-write18  -interaction=nonstopmode -file-line-error';
# $latex= "pdflatex -shell-escape  %O %S";
$xelatex= "xelatex -no-pdf  -shell-escape %O %S";

# Clean prend en compote les fichiers générés par latex
# $cleanup_includes_generated=1;
push @generated_exts, 'synctex', 'synctex.gz';

# Recorder liste les fichiers
$recorder=1;

# Pour gnuplot ?
use File::Basename;


add_cus_dep('svg', 'pdf_tex', 0 , 'svg2pdf_tex');
sub svg2pdf_tex {
	return system("inkscape --export-area-page --export-dpi=300 --export-latex --export-type=pdf \"$_[0].svg\"");
}

add_cus_dep('gnuplot','plot_tex',0,'gnuplot2tex');
sub gnuplot2tex {
	system("gnuplot -e \"cd '" . dirname("$_[0].gnuplot") . "'\" -e \"set output '" . basename("$_[0]") . ".tex'\" " . basename("$_[0].gnuplot")) and die "Gnuplot call failed";
	return rename "$_[0].tex","$_[0].plot_tex";
}


#!/usr/bin/gnuplot -persist
# vi:syntax=gnuplot 

reset

set terminal cairolatex pdf size 6,4 #mono
set palette cubehelix

unset key

set loadpath '~/.config/gnuplot-config/latex'
load 'xyborder.cfg'
load 'grid.cfg'

set xlabel '<++>'
set ylabel '<++>'

#set xrange [1:12]
#set xtics 1

<++>

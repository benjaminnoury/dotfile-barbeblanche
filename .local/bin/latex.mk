TEX="pdflatex -shell-escape -interaction=nonstopmode -file-line-error"

SOURCE=$(wildcard *.tex)

# Images
SVG=$(wildcard img/*.svg:.svg=.pdf_tex)

# Courbes
GNUPLOT=$(wildcard img/*.gnuplot:.gnuplot=plot_tex)

# Document final
FINAL=$(SOURCE:.tex=.pdf)

# Fichiers de style, etc
MKfile=/home/ben/.local/bin/latex.mk
STY=$(wildcard /home/ben/texmf/tex/latex/perso/*.sty)

all: $(FINAL)

%.pdf : %.tex $(SVG) $(GNUPLOT) $(STY) $(MKfile) %.aux
	$(TEX) $<

img/:
	mkdir img

img/%.plot_tex: img/%.gnuplot
	@echo "$< --> $@"
	@gnuplot -e "set output '$(@:plot_tex=tex)'" $<
	@mv $(@:plot_tex=tex) $@

%.plot_tex: %.gnuplot
	@echo "$< --> $@"
	@gnuplot -e "set output '$(@:plot_tex=tex)'" $<
	@mv $(@:plot_tex=tex) $@

img/%.pdf_tex : img/%.svg
	@echo "$< --> $@"
	@inkscape --export-area-page --export-dpi=300 --export-latex --export-type=pdf $<

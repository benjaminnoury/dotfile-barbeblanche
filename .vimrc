filetype off

"""""""""""""""
""" Plugins """
"""""""""""""""
call plug#begin('~/.vim/plugged')

" Plug 'francoiscabrol/ranger.vim'  
" let g:ranger_replace_netrw = 1

Plug 'itchyny/lightline.vim'
Plug 'morhetz/gruvbox'
" Plug 'arcticicestudio/nord-vim'
Plug 'dylanaraps/wal.vim'


Plug 'lilydjwg/colorizer'

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-vinegar'

Plug 'Raimondi/delimitMate'
let delimitMate_expand_cr = 1
augroup mydelimitMate
	au!
	au FileType markdown let b:delimitMate_nesting_quotes = ["`"]
	au FileType tex let b:delimitMate_quotes = ""
	au FileType tex let b:delimitMate_matchpairs = "(:),[:],{:},`:'"
	au FileType python let b:delimitMate_nesting_quotes = ['"', "'"]
augroup END

" Plug 'tpope/vim-abolish'

Plug 'tpope/vim-commentary'

Plug 'tpope/vim-surround'

Plug 'lervag/vimtex'

Plug 'vim-scripts/AutoComplPop'

" Plug 'sirver/ultisnips'
" let g:UltiSnipsExpandTrigger           = '<tab>'
" let g:UltiSnipsJumpForwardTrigger      = '<tab>'
" let g:UltiSnipsJumpBackwardTrigger     = '<s-tab>'

Plug 'brennier/quicktex'

Plug 'sheerun/vim-polyglot'

Plug 'vimwiki/vimwiki'

Plug 'justinmk/vim-dirvish'
Plug 'kristijanhusak/vim-dirvish-git'

Plug 'tpope/vim-fugitive'

Plug 'AndrewRadev/id3.vim'

call plug#end()
filetype plugin indent on

""""""""""""""""""""""
"" General settings ""
""""""""""""""""""""""
let mapleader=","
let maplocalleader=';'
set backspace=indent,eol,start
set number relativenumber
set encoding=utf-8
syntax on
set mouse=a
set incsearch
set ignorecase
set smartcase
set hlsearch

nnoremap <leader><Enter> :silent! nohls<cr>

" désactive les commentaires automatiques
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

""" FZF
let g:fzf_layout = { 'down': '~40%' }
inoremap <C-t> <ESC>:w<CR>:FZF<CR>
noremap  <C-t> <ESC>:w<CR>:FZF<CR>
noremap  è <ESC>:w<CR>:FZF<CR>
inoremap <C-f> <ESC>:Lines<CR>
noremap  <C-f> <ESC>:Lines<CR>
" selection d'un buffer avec ,b ou CTRL + B
nnoremap  <C-b> :w<CR>:Buffers<CR>
nnoremap  <leader>b :w<CR>:Buffers<CR>
inoremap  <C-b> <ESC>:w<CR>:Buffers<CR>
nnoremap <leader>h :w<CR>:History<CR>

""" Colorscheme
let g:gruvbox_contrast_dark='medium'
colorscheme gruvbox
set background=dark
set laststatus=2
let g:lightline = {'colorscheme': 'gruvbox'}
hi Normal ctermbg=None
" colorscheme wal

""" Plein de façons de centrer
nnoremap J jzz
nnoremap K kzz
nnoremap n nzz
autocmd InsertEnter * norm zz

""" j et k sur Visual Lines
nnoremap <expr> k  (v:count? 'k': 'gk')
nnoremap <expr> j  (v:count? 'j': 'gj')

""" Suivre le curseur
set cursorcolumn
set cursorline

" Vérification du langage avec F6
nnoremap <F6> :setlocal spell! spelllang=fr<CR>

" ² enregistre le buffer et passe au suivant
nnoremap ² :w<CR>:b#<CR>

" F9 enregistre, quelque soit le mode
noremap  <F9> <Esc>:w<CR>
inoremap <F9> <Esc>:w<CR>i
vnoremap <F9> <Esc>:w<CR>

""" Le répertoire courant suit le buffer
autocmd BufEnter * silent! lcd %:p:h

""" Find dans les sous dossiers --- Peu utilisé depuis fzf
set path+=**
" Wildmenu comme zsh
set wildmenu
set wildmode=longest,list,full

""" Alternatives à escape
inoremap jk <esc>
inoremap kj <esc>
inoremap kk <esc>
inoremap jj <esc>


"""""""""""""""""
""" Templates """
"""""""""""""""""
" Copie un template (s'il existe) lors de l'ouverture d'un nouveau fichier
autocmd BufNewFile * silent! 0r ~/Modèles/%:e.tpl
autocmd BufNewFile Makefile silent! 0r ~/Modèles/Makefile.tpl

" PlaceHolders dans les Modèles
vnoremap <Space><Tab> <ESC>/<Enter>"_c4l
inoremap <Space><Tab> <ESC>/<++><Enter>"_c4l
noremap <Space><Tab> <ESC>/<++><Enter>"_c4l

""""""""""""""""
""" Autocmds """
""""""""""""""""
" commandes à exécuter automatiquement lors de l'enregistrement de fichier
autocmd BufWritePost ~/.Xresources,~/.Xdefaults !xrdb %

" les notes du répertoires notes sont considérés du Markdown
autocmd BufRead,BufNewFile ~/vimwiki/* set filetype=markdown
autocmd BufRead,BufNewFile *.tikz set filetype=tex


""" Copier-coller dans le clipboard système"""""""""""""""""""""""""""
vnoremap <C-c> "*Y :let @+=@<CR>
map <C-p> "+P


""""""""""""""""""
""" Pour NetRW """
""""""""""""""""""
let g:netrw_banner=0		" désactive l'entête
let g:netrw_liststyle=3		" tree view
let g:netrw_browse_split=3	" ouvrir au premier plan
let g:netrw_altv=1		" ouvre les splits sur la droite
let g:netrw_winsize=25	" 25%
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'
" augroup ProjectDrawer
" 	autocmd!
" 	autocmd VimEnter * :Vexplore
" augroup END

"""""""""""""""""""""""""""
"""  Gestion des splits """
"""""""""""""""""""""""""""
set splitbelow
set splitright

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

"""""""""""""""""""""""""""""""""
""" Config spécifique à latex """
"""""""""""""""""""""""""""""""""

""" vimtex
let g:tex_flavor='latex'
let g:vimtex_view_method='zathura'
let g:vimtex_complete_enabled=1
let g:vimtex_quickfix_mode=0
set conceallevel=0
let g:tex_conceal="abmg"

nnoremap é <Esc>:VimtexTocOpen<CR>
""" Inkscape
inoremap <C-f> <Esc>:exec '.!vimskape "'.getline('.').'"'<CR><CR>
nnoremap <C-f> call fzf#run(fzf#wrap({'source':'fd .svg ./img','sink':'rifle'}))


" A priori abandonné et remplacé par latexmk
" Texmake est un makefile perso pour les documents Pandoc/Tex/gnuplot
" au FileType markdown,tex,gnuplot,tikz noremap  <F8> <Esc>:w<CR>:!texmake %<CR><CR>
" au FileType markdown,tex,gnuplot,tikz inoremap  <F8> <Esc>:w<CR>:!texmake %<CR><CR>
" au FileType markdown,tex,gnuplot,tikz vnoremap  <F8> <Esc>:w<CR>:!texmake %<CR><CR>

" Accès plus aisé à \
au FileType markdown,tex,gnuplot inoremap ù \
au FileType markdown,tex,gnuplot inoremap uu ù

autocmd FileType tex set makeprg=make\ -f\ ~/.local/bin/latex.mk\ %

" Efface les fichiers de constructions en sortant d'un .tex ou d'un .md
autocmd VimLeave *.tex !latexmk -c %

let g:quicktex_tex = {
	\' '   	: "\<ESC>:call search('<+.*+>')\<CR>\"_c/+>/e\<CR>",
	\'m'   	: '\( <+++> \) <++>',
	\'...'	: "\ldots <+++>",
	\'table' : "\begin{table}\<CR>\centering\<CR>\caption{<++>}\<CR>\label{<++>}\<CR>",
	\'im'	:	'\includegraphics[width=<++>\textwidth]{img/<+++>}',
\}

#!/usr/bin/gnuplot -persist
# vi:syntax=gnuplot 

reset

set terminal cairolatex pdf size 6,4 #mono
set palette cubehelix

unset key

# Réglage bordure
# set style line 101 lc rgb '#808080' lt 1 lw 1
# set border 3 front ls 101
set tics  out scale 0.75
set format '\num{%g}'
####

# Réglage Grid
set grid xtics ytics ls 3 lw 1 lc rgb 'gray'
##

set xlabel '<++>'
set ylabel '<++>'

#set xrange [1:12]
#set xtics 1
#set mxtics

<++>

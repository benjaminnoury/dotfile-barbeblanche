\documentclass[crop,tikz,multi=false]{standalone}
%\documentclass[crop,multi=false]{standalone}

\usepackage{notations}
\RequirePackage{fontspec}
\RequirePackage{xunicode}
\RequirePackage[libertine]{newtxmath}
\setmainfont{Linux Libertine O}
\usepackage{tikz}
%\usepackage[europeanresistors,americaninductors]{circuitikzgit}
\usetikzlibrary{patterns,arrows,shapes}
\tikzstyle{bati}=[pattern=north west lines]


\begin{document}
\begin{tikzpicture}[>=stealth']
<++>
;;\end{tikzpicture}
\end{document}
